﻿using Assignment_5_CSharp_Characters.Characters.Classes;
using Assignment_5_CSharp_Characters.Items.ItemTypes;
using System;

namespace Assignment_5_CSharp_Characters
{
    /// <summary>
    /// This is the class that was used as the original runtime when coding
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            //Create the different classes
            Mage mage1 = new Mage
            {
                Name = "Albert",
                CharacterClass = "Mage",

                VitalityBase = 5,
                StrengthBase = 1,
                DexterityBase = 1,
                IntelligenceBase = 8,

                VitalityPerLevel = 3, 
                StrengthPerLevel = 1,
                DexterityPerLevel = 1,
                IntelligencePerLevel = 5
            };

            Ranger ranger1 = new Ranger
            {
                Name = "Robin",
                CharacterClass = "Ranger",

                VitalityBase = 8,
                StrengthBase = 1,
                DexterityBase = 7,
                IntelligenceBase = 1,

                VitalityPerLevel = 2,
                StrengthPerLevel = 2,
                DexterityPerLevel = 5,
                IntelligencePerLevel = 1
            };

            Rogue rogue1 = new Rogue
            {
                Name = "Alan",
                CharacterClass = "Rogue",

                VitalityBase = 8,
                StrengthBase = 2,
                DexterityBase = 6,
                IntelligenceBase = 1,

                VitalityPerLevel = 3,
                StrengthPerLevel = 1,
                DexterityPerLevel = 4,
                IntelligencePerLevel = 1
            };

            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            //Create Weapons
            Weapon testAxe = new Weapon
            {
                ItemName = "testAxe",
              //ItemLevel = 2,
                ItemSlot = "Weapon",

                WeaponType = "Axe",
                Damage = 11,
                AttackSpeed = 0.8
            };

            Armour testLeather = new Armour
            {
                ItemName = "testLeather",
             // ItemLevel = 3,
                ItemSlot = "Body",

                ArmourType = "Leather",
                VitalityBonus = 0,
                StrengthBonus = 2,
                DexterityBonus = 2,
                IntelligenceBonus = 0

            };
            
            Armour testPlate = new Armour
            {
                ItemName = "testPlate",
                // ItemLevel = 4,
                ItemSlot = "Body",

                ArmourType = "Plate",
                VitalityBonus = 5,
                StrengthBonus = 5,
                DexterityBonus = 0,
                IntelligenceBonus = 0

            };

            //Runtime logging

            //Testing level
            //mage1.ReturnLevel();
            //ranger1.ReturnLevel();
            //rogue1.ReturnLevel();
            //warrior1.ReturnLevel();

            //Testing levelup
            //mage1.LevelUp(-1);
            //ranger1.LevelUp(0);
            //rogue1.LevelUp(1);
            //warrior1.LevelUp(2);

            //Testing weapon type & already equipped in slot
            //testAxe.TryEquipWeapon(mage1);
            //testAxe.TryEquipWeapon(warrior1);
            //testAxe.TryEquipWeapon(warrior1);

            //Testing weapon level
            //testAxe.TryEquipWeapon(warrior1);
            //warrior1.LevelUp(2);
            //testAxe.TryEquipWeapon(warrior1);

            //Testing armor type & already equipped in slot
            //testLeather.TryEquipArmour(warrior1);
            //testLeather.TryEquipArmour(mage1);
            //testLeather.TryEquipArmour(ranger1);

            //Testing  armor level
            //testLeather.TryEquipArmour(rogue1);
            //rogue1.LevelUp(2);
            //testLeather.TryEquipArmour(rogue1);

            //Testing stat and DPS calculation
            testAxe.TryEquipWeapon(warrior1);
            testPlate.TryEquipArmour(warrior1);
            warrior1.CalculateDPS();

            //Testing character info display
            warrior1.CharacterDisplay();
        }
    }
}
