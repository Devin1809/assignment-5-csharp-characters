﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5_CSharp_Characters.Characters
{
    public abstract class Character
    {
        //Values
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public string CharacterClass { get; set; }

        public double VitalityBase { get; set; }
        public double StrengthBase { get; set; }
        public double DexterityBase { get; set; }
        public double IntelligenceBase { get; set; }

        public double VitalityPerLevel { get; set; }
        public double StrengthPerLevel { get; set; }
        public double DexterityPerLevel { get; set; }
        public double IntelligencePerLevel { get; set; }

        public double VitalityEquip { get; set; } = 0;
        public double StrengthEquip { get; set; } = 0;
        public double DexterityEquip { get; set; } = 0;
        public double IntelligenceEquip { get; set; } = 0;

        public double VitalityTotal { get; set; }
        public double StrengthTotal { get; set; }
        public double DexterityTotal { get; set; }
        public double IntelligenceTotal { get; set; }

        public double Health { get; set; }
        public double ArmorRating { get; set; }
        public double ElementalResistance { get; set; }

        public bool WeaponEquipped { get; set; } = false;
        public bool HeadEquipped { get; set; } = false;
        public bool BodyEquipped { get; set; } = false;
        public bool LegsEquipped { get; set; } = false;

        public double BaseDPS { get; set; }
        public double WeaponDamage { get; set; } = 1;
        public double WeaponAttackSpeed { get; set; } = 1;
        public double WeaponDPS { get; set; }
        public double TotalDPS { get; set; }

        /// <summary>
        /// Checks the current level of the character
        /// </summary>
        public int ReturnLevel()
        {
            Console.WriteLine(CharacterClass + "'s level is " + Level);
            return Level;
        }

        /// <summary>
        /// Purely made to return the primary stats in test cases 4 + 5
        /// </summary>
        /// <returns></returns>
        public double[] ReturnPrimStats()
        {
            double[] baseStats = new double[] { VitalityBase, StrengthBase, DexterityBase, IntelligenceBase };
            return baseStats;
        }

        /// <summary>
        /// Purely made to return the weapon succes message in test case 5
        /// </summary>
        /// <returns></returns>
        public string ReturnWeaponEquipped()
        {
            if(WeaponEquipped == true)
            {
                return "New weapon equipped!";
            }
            else
            {
                return "New weapon was not equipped!";
            }
        }

        /// <summary>
        /// Purely made to return the weapon succes message in test case 6
        /// </summary>
        /// <returns></returns>
        public string ReturnHeadEquipped()
        {
            if (HeadEquipped == true)
            {
                return "New armour equipped!";
            }
            else
            {
                return "New armour was not equipped!";
            }
        }
        /// <summary>
        /// Purely made to return the weapon succes message in test case 6
        /// </summary>
        /// <returns></returns>
        public string ReturnBodyEquipped()
        {
            if (BodyEquipped == true)
            {
                return "New armour equipped!";
            }
            else
            {
                return "New armour was not equipped!";
            }
        }

        /// <summary>
        /// Purely made to return the weapon succes message in test case 6
        /// </summary>
        /// <returns></returns>
        public string ReturnLegsEquipped()
        {
            if (LegsEquipped == true)
            {
                return "New armour equipped!";
            }
            else
            {
                return "New armour was not equipped!";
            }
        }


        /// <summary>
        /// Handles the base stat changes of leveling up
        /// </summary>
        /// <param name="amount"></param>
        public void LevelUp(int amount)
        {
            if(amount > 0)
            {
                Level += amount;

                VitalityBase += VitalityPerLevel * amount;
                StrengthBase += StrengthPerLevel * amount;
                DexterityBase += DexterityPerLevel * amount;
                IntelligenceBase += IntelligencePerLevel * amount;

                Console.WriteLine(CharacterClass + " level was increased by " + amount);
                Console.WriteLine("New " + CharacterClass + " stats at level " + Level + ":");
                Console.WriteLine("Vitality: " + VitalityBase);
                Console.WriteLine("Strength: " + StrengthBase);
                Console.WriteLine("Dexterity: " + DexterityBase);
                Console.WriteLine("Intelligence: " + IntelligenceBase);

                CalculateStats();
            }
            else
            {
                Console.WriteLine("You can't increase your level by " + amount);
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Handles the total stat changes of leveling up
        /// </summary>
        public void CalculateStats()
        {
            VitalityTotal = VitalityBase + VitalityEquip;
            StrengthTotal = StrengthBase + StrengthEquip;
            DexterityTotal = DexterityBase + DexterityEquip;
            IntelligenceTotal = IntelligenceBase + IntelligenceEquip;

            Health = VitalityTotal * 10;
            ArmorRating = StrengthTotal + DexterityTotal;
            ElementalResistance = IntelligenceTotal;
        }

        /// <summary>
        /// Purely made to return the secondary stats in test case 6
        /// </summary>
        /// <returns></returns>
        public double[] ReturnSecondStats()
        {
            double[] secondStats = new double[] { Health, ArmorRating, ElementalResistance };
            return secondStats;
        }

        /// <summary>
        /// Calculates then logs the Base, Weapon and Total DPS Values
        /// </summary>
        public void CalculateDPS()
        {
            CalculateStats();
            switch (CharacterClass)
            {
                case "Mage":
                    BaseDPS = 1 + IntelligenceBase / 100;
                    break;
                case "Ranger":
                case "Rogue":
                    BaseDPS = 1 + DexterityBase / 100;
                    break;
                case "Warrior":
                    BaseDPS = 1 + StrengthBase / 100;
                    break;
                default:
                    break;
            }
            Console.WriteLine("BaseDPS: " + Math.Round(BaseDPS, 2));

            WeaponDPS = (WeaponDamage * WeaponAttackSpeed) * BaseDPS;
            Console.WriteLine("WeaponDPS: " + Math.Round(WeaponDPS, 2));

            switch (CharacterClass)
            {
                case "Mage":
                    TotalDPS = (WeaponDamage * WeaponAttackSpeed) * (1 + IntelligenceTotal / 100);
                    break;
                case "Ranger":
                case "Rogue":
                    TotalDPS = (WeaponDamage * WeaponAttackSpeed) * (1 + DexterityTotal / 100);
                    break;
                case "Warrior":
                    TotalDPS = (WeaponDamage * WeaponAttackSpeed) * (1 + StrengthTotal / 100);
                    break;
                default:
                    break;
            }
            Console.WriteLine("TotalDPS: " + Math.Round(TotalDPS, 2));
        }

        /// <summary>
        /// Purely made to return the baseDPS in test case 7
        /// </summary>
        /// <returns></returns>
        public double ReturnBaseDPS()
        {
            return Math.Round(BaseDPS, 2);
        }

        /// <summary>
        /// Purely made to return the weaponDPS in test case 8
        /// </summary>
        /// <returns></returns>
        public double ReturnWeaponDPS()
        {
            return Math.Round(WeaponDPS, 2);
        }

        /// <summary>
        /// Purely made to return the totalDPS in test case 9
        /// </summary>
        /// <returns></returns>
        public double ReturnTotalDPS()
        {
            return Math.Round(TotalDPS, 2);
        }

        /// <summary>
        /// Displays the character info from appendix B5
        /// </summary>
        public void CharacterDisplay()
        {
            Console.WriteLine("Character info:");
            Console.WriteLine("Character name: " + Name);
            Console.WriteLine("Character level: " + Level);
            Console.WriteLine("Character class: " + CharacterClass);
            Console.WriteLine("Strength: " + StrengthTotal);
            Console.WriteLine("Dexterity: " + DexterityTotal);
            Console.WriteLine("Intelligence: " + IntelligenceTotal);
            Console.WriteLine("Health: " + Health);
            Console.WriteLine("Armor Rating: " + ArmorRating);
            Console.WriteLine("Elemental Resistance: " + ElementalResistance);
            Console.WriteLine("DPS: " + Math.Round(TotalDPS, 2));
        }
    }
}
