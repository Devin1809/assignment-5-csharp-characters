﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5_CSharp_Characters.Custom_Exceptions
{
    public class InvalidArmourException : Exception
    {
        public InvalidArmourException(string message) : base(message)
        {
        }
    }
}
