﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5_CSharp_Characters.Items
{
    public class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public string ItemSlot { get; set; }
    }
}
