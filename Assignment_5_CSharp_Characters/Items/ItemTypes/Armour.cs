﻿using Assignment_5_CSharp_Characters.Characters;
using Assignment_5_CSharp_Characters.Custom_Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5_CSharp_Characters.Items.ItemTypes
{
    public class Armour : Item
    {
        public string ArmourType { get; set; }
        public int VitalityBonus { get; set; }
        public int StrengthBonus { get; set; }
        public int DexterityBonus { get; set; }
        public int IntelligenceBonus { get; set; }

        /// <summary>
        /// Purely made to return the InvalidArmour Exception in test cases 2 + 4
        /// </summary>
        /// <param name="hero"></param>
        public void ThrowInvalidArmourException(Character hero)
        {
            throw new InvalidArmourException("Your character cannot equip this armor piece");
        }

        /// <summary>
        /// Check if the Character's level is high enough to equip the item
        /// </summary>
        /// <param name="hero"></param>
        public void TryEquipArmour(Character hero)
        {
            if (ItemLevel <= hero.Level)
            {
                CheckMatchingClass(hero);
            }
            else
            {
                Console.WriteLine("Your character level is too low to equip the " + ItemName + "!");
                ThrowInvalidArmourException(hero);
            }
        }

        /// <summary>
        /// Check if the Character's class type is compatible with the Armour type
        /// </summary>
        /// <param name="hero"></param>
        public void CheckMatchingClass(Character hero)
        {
            switch (ArmourType)
            {
                case "Cloth":
                    if (hero.CharacterClass == "Mage")
                    {
                        CheckExistingArmour(hero);
                    }
                    else
                    {
                        LogWrongArmour(hero);
                    }
                    break;
                case "Plate":
                    if (hero.CharacterClass == "Warrior")
                    {
                        CheckExistingArmour(hero);
                    }
                    else
                    {
                        LogWrongArmour(hero);
                    }
                    break;
                case "Leather":
                    if (hero.CharacterClass == "Ranger" || hero.CharacterClass == "Rogue")
                    {
                        CheckExistingArmour(hero);
                    }
                    else
                    {
                        LogWrongArmour(hero);
                    }
                    break;
                case "Mail":
                    if (hero.CharacterClass == "Ranger" || hero.CharacterClass == "Rogue" || hero.CharacterClass == "Warrior")
                    {
                        CheckExistingArmour(hero);
                    }
                    else
                    {
                        LogWrongArmour(hero);
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// If the armor type is compatible with the Character Class, equip it in the desired slot if that slot is empty
        /// </summary>
        /// <param name="hero"></param>
        public void CheckExistingArmour(Character hero)
        {
            switch (ItemSlot)
            {
                case "Head":
                    if (hero.HeadEquipped == false)
                    {
                        LogEquipArmour(hero);
                        hero.HeadEquipped = true;
                    }
                    else
                    {
                        LogExistingArmour(hero);
                    }
                    break;
                case "Body":
                    if (hero.BodyEquipped == false)
                    {
                        LogEquipArmour(hero);
                        hero.BodyEquipped = true;
                    }
                    else
                    {
                        LogExistingArmour(hero);
                    }
                    break;
                case "Legs":
                    if (hero.LegsEquipped == false)
                    {
                        LogEquipArmour(hero);
                        hero.LegsEquipped = true;
                    }
                    else
                    {
                        LogExistingArmour(hero);
                    }
                    break;
            }
        }

        /// <summary>
        /// If the armour type is not compatible with the Character Class, log a message to tell us that
        /// </summary>
        /// <param name="hero"></param>
        public void LogWrongArmour(Character hero)
        {
            Console.WriteLine(ArmourType + "s cannot be equipped to a " + hero.CharacterClass + "!");
            ThrowInvalidArmourException(hero);
        }


        /// <summary>
        /// Equip the armour to the Character
        /// </summary>
        /// <param name="hero"></param>
        public void LogEquipArmour(Character hero)
        {
            hero.VitalityEquip = VitalityBonus;
            hero.StrengthEquip = StrengthBonus;
            hero.DexterityEquip = DexterityBonus;
            hero.IntelligenceEquip = IntelligenceBonus;
            Console.WriteLine(hero.Name + " the " + hero.CharacterClass + " has equipped " + ItemName);
        }

        /// <summary>
        /// If the Character already has armour equipped in the desired slot, log a message to tell us that
        /// </summary>
        /// <param name="hero"></param>
        public void LogExistingArmour(Character hero)
        {
            Console.WriteLine(hero.Name + " the " + hero.CharacterClass + " already has equipment in the " + ItemSlot + " slot!");
        }

    }
}
