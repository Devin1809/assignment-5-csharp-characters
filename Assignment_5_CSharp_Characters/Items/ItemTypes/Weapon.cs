﻿using Assignment_5_CSharp_Characters.Characters;
using Assignment_5_CSharp_Characters.Custom_Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_5_CSharp_Characters.Items.ItemTypes
{
    public class Weapon : Item
    {
        public string WeaponType { get; set; }
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// Purely made to return the InvalidWeaponException in test cases 1 + 3
        /// </summary>
        /// <param name="hero"></param>
        public void ThrowInvalidWeaponException(Character hero)
        {
            throw new InvalidWeaponException("You cannot equip this weapon");
        }

        /// <summary>
        /// Check if the Character's level is high enough to equip the item
        /// </summary>
        /// <param name="hero"></param>
        public void TryEquipWeapon(Character hero)
        { 
            if(ItemLevel <= hero.Level)
            {
                this.CheckMatchingClass(hero);
            }
            else
            {
                Console.WriteLine("Your character level is too low to equip the " + ItemName + "!");
                ThrowInvalidWeaponException(hero);
            }

        }

        /// <summary>
        /// Check if the Character's class type is compatible with the Weapon type
        /// </summary>
        /// <param name="hero"></param>
        public void CheckMatchingClass(Character hero)
        {
            switch (WeaponType)
            {
                case "Staff":
                case "Wand":
                    if (hero.CharacterClass == "Mage")
                    {
                        CheckExistingWeapon(hero);
                    }
                    else
                    {
                        LogWrongWeapon(hero);
                    }
                    break;
                case "Bow":
                    if (hero.CharacterClass == "Ranger")
                    {
                        CheckExistingWeapon(hero);
                    }
                    else
                    {
                        LogWrongWeapon(hero);
                    }
                    break;
                case "Dagger":
                    if (hero.CharacterClass == "Rogue")
                    {
                        CheckExistingWeapon(hero);
                    }
                    else
                    {
                        LogWrongWeapon(hero);
                    }
                    break;
                case "Axe":
                case "Hammer":
                    if (hero.CharacterClass == "Warrior")
                    {
                        CheckExistingWeapon(hero);
                    }
                    else
                    {
                        LogWrongWeapon(hero);
                    }
                    break;
                case "Sword":
                    if (hero.CharacterClass == "Rogue" || hero.CharacterClass == "Warrior")
                    {
                        CheckExistingWeapon(hero);
                    }
                    else
                    {
                        LogWrongWeapon(hero);
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// If the weapon type is compatible with the Character Class, equip it in the weapon slot if it is empty
        /// </summary>
        /// <param name="hero"></param>
        public void CheckExistingWeapon(Character hero)
        {
            if (hero.WeaponEquipped == false)
            { 
                hero.WeaponDamage = Damage;
                hero.WeaponAttackSpeed = AttackSpeed;
                Console.WriteLine(hero.Name + " the " + hero.CharacterClass + " has equipped " + ItemName);
              //Console.WriteLine("Weapon Damage: " + hero.WeaponDamage);
              //Console.WriteLine("Weapon Attack Speed: " + hero.WeaponAttackSpeed);
                hero.WeaponEquipped = true;
            }
            else
            {
                Console.WriteLine(hero.Name + " the " + hero.CharacterClass + " already has a weapon!");
            }
        }

        /// <summary>
        /// If the weapon type is not compatible with the Character Class, log a message to tell us that
        /// </summary>
        /// <param name="hero"></param>
        public void LogWrongWeapon(Character hero)
        {
            Console.WriteLine(WeaponType + "s cannot be equipped to a " + hero.CharacterClass + "!");
            ThrowInvalidWeaponException(hero);
        }
    }
}
