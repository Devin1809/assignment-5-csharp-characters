using System;
using Xunit;
using Assignment_5_CSharp_Characters;
using Assignment_5_CSharp_Characters.Characters.Classes;
using Assignment_5_CSharp_Characters.Items.ItemTypes;
using Assignment_5_CSharp_Characters.Custom_Exceptions;

namespace programTest
{
    /// <summary>
    /// The needed data from program was recreated here for more efficient testing
    /// </summary>
    public class UnitTestCharacter
    {
        /// <summary>
        /// This class contains all Character test cases
        /// </summary>
        //Test case 1:
        [Fact]
        public void Get_CharacterLevel_ShouldReturnLevel1()
        {
            //Arrange
            Warrior warrior1 = new Warrior { };

            int level = 1;
            int expected = level;
            //Act
            int actual = warrior1.ReturnLevel();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 2: 
        [Fact]
        public void Increase_CharacterLevelBy1_ShouldReturnLevel2()
        {
            //Arrange
            Warrior warrior1 = new Warrior { };

            int level = 1;
            int amountToLevel = 1;
            int expected = level + amountToLevel;
            //Act
            warrior1.LevelUp(amountToLevel);
            int actual = warrior1.ReturnLevel();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 3:
        [Fact]
        public void Increase_CharacterLevelByZeroOrLess_ShouldThrowArgumentException()
        {
            //Arrange
            Warrior warrior1 = new Warrior { };

            int amountToLevel = 0;
            //Act + Assert
            Assert.Throws<ArgumentException>(() => warrior1.LevelUp(amountToLevel));
        }

        //Test case 4.1:
        [Fact]
        public void Get_MageLevel1Stats_ShouldReturnMagePrimaryAttributes()
        {
            //Arrange
            Mage mage1 = new Mage
            {
                Name = "Albert",
                CharacterClass = "Mage",

                VitalityBase = 5,
                StrengthBase = 1,
                DexterityBase = 1,
                IntelligenceBase = 8,

                VitalityPerLevel = 3,
                StrengthPerLevel = 1,
                DexterityPerLevel = 1,
                IntelligencePerLevel = 5
            };

            double vitality = 5;
            double strength = 1;
            double dexterity = 1;
            double intelligence = 8;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            //Act
            double[] actual = mage1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 4.2:
        [Fact]
        public void Get_RangerLevel1Stats_ShouldReturnRangerPrimaryAttributes()
        {
            //Arrange
            Ranger ranger1 = new Ranger
            {
                Name = "Robin",
                CharacterClass = "Ranger",

                VitalityBase = 8,
                StrengthBase = 1,
                DexterityBase = 7,
                IntelligenceBase = 1,

                VitalityPerLevel = 2,
                StrengthPerLevel = 2,
                DexterityPerLevel = 5,
                IntelligencePerLevel = 1
            };

            double vitality = 8;
            double strength = 1;
            double dexterity = 7;
            double intelligence = 1;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            //Act
            double[] actual = ranger1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 4.3:
        [Fact]
        public void Get_RogueLevel1Stats_ShouldReturnRoguePrimaryAttributes()
        {
            //Arrange
            Rogue rogue1 = new Rogue
            {
                Name = "Alan",
                CharacterClass = "Rogue",

                VitalityBase = 8,
                StrengthBase = 2,
                DexterityBase = 6,
                IntelligenceBase = 1,

                VitalityPerLevel = 3,
                StrengthPerLevel = 1,
                DexterityPerLevel = 4,
                IntelligencePerLevel = 1
            };

            double vitality = 8;
            double strength = 2;
            double dexterity = 6;
            double intelligence = 1;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            //Act
            double[] actual = rogue1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 4.4:
        [Fact]
        public void Get_WarriorLevel1Stats_ShouldReturnWarriorPrimaryAttributes()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            double vitality = 10;
            double strength = 5;
            double dexterity = 2;
            double intelligence = 1;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            //Act
            double[] actual = warrior1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 5.1:
        [Fact]
        public void Get_MageLevel2Stats_ShouldReturnMagePrimaryAttributes()
        {
            //Arrange
            Mage mage1 = new Mage
            {
                Name = "Albert",
                CharacterClass = "Mage",

                VitalityBase = 5,
                StrengthBase = 1,
                DexterityBase = 1,
                IntelligenceBase = 8,

                VitalityPerLevel = 3,
                StrengthPerLevel = 1,
                DexterityPerLevel = 1,
                IntelligencePerLevel = 5
            };

            double vitality = 8;
            double strength = 2;
            double dexterity = 2;
            double intelligence = 13;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            int amountToLevel = 1;

            //Act
            mage1.LevelUp(amountToLevel);
            double[] actual = mage1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 5.2:
        [Fact]
        public void Get_RangerLevel2Stats_ShouldReturnRangerPrimaryAttributes()
        {
            //Arrange
            Ranger ranger1 = new Ranger
            {
                Name = "Robin",
                CharacterClass = "Ranger",

                VitalityBase = 8,
                StrengthBase = 1,
                DexterityBase = 7,
                IntelligenceBase = 1,

                VitalityPerLevel = 2,
                StrengthPerLevel = 2,
                DexterityPerLevel = 5,
                IntelligencePerLevel = 1
            };

            double vitality = 10;
            double strength = 3;
            double dexterity = 12;
            double intelligence = 2;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            int amountToLevel = 1;

            //Act
            ranger1.LevelUp(amountToLevel);
            double[] actual = ranger1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 5.3:
        [Fact]
        public void Get_RogueLevel2Stats_ShouldReturnRoguePrimaryAttributes()
        {
            //Arrange
            Rogue rogue1 = new Rogue
            {
                Name = "Alan",
                CharacterClass = "Rogue",

                VitalityBase = 8,
                StrengthBase = 2,
                DexterityBase = 6,
                IntelligenceBase = 1,

                VitalityPerLevel = 3,
                StrengthPerLevel = 1,
                DexterityPerLevel = 4,
                IntelligencePerLevel = 1
            };

            double vitality = 11;
            double strength = 3;
            double dexterity = 10;
            double intelligence = 2;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            int amountToLevel = 1;

            //Act
            rogue1.LevelUp(amountToLevel);
            double[] actual = rogue1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 5.4:
        [Fact]
        public void Get_WarriorLevel2Stats_ShouldReturnWarriorPrimaryAttributes()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            double vitality = 15;
            double strength = 8;
            double dexterity = 4;
            double intelligence = 2;
            double[] expected = new double[] { vitality, strength, dexterity, intelligence };

            int amountToLevel = 1;

            //Act
            warrior1.LevelUp(amountToLevel);
            double[] actual = warrior1.ReturnPrimStats();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 6:
        [Fact]
        public void Get_WarriorLevel2Stats_ShouldReturnWarriorSecondaryAttributes()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            double health = 150;
            double armorRating = 12;
            double elementalResistance = 2;
            double[] expected = new double[] { health, armorRating, elementalResistance };

            int amountToLevel = 1;

            //Act
            warrior1.LevelUp(amountToLevel);
            double[] actual = warrior1.ReturnSecondStats();
            //Assert
            Assert.Equal(expected, actual);
        }

    }

    public class UnitTestEquipment
    {
        /// <summary>
        /// This class contains all Equipment test cases
        /// </summary>
        //Test case 1:
        [Fact]
        public void Equip_TooHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Weapon testAxe = new Weapon
            {
                ItemName = "testAxe",
                ItemLevel = 2,
                ItemSlot = "Weapon",

                WeaponType = "Axe",
                Damage = 11,
                AttackSpeed = 0.8
            };

            //Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testAxe.TryEquipWeapon(warrior1));
        }

        //Test case 2:
        [Fact]
        public void Equip_TooHighLevelArmour_ShouldThrowInvalidArmourException()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Armour testPlate = new Armour
            {
                ItemName = "testPlate",
                ItemLevel = 2,
                ItemSlot = "Body",

                ArmourType = "Plate",
                VitalityBonus = 5,
                StrengthBonus = 5,
                DexterityBonus = 0,
                IntelligenceBonus = 0

            };

            //Act + Assert
            Assert.Throws<InvalidArmourException>(() => testPlate.TryEquipArmour(warrior1));
        }

        //Test case 3:
        [Fact]
        public void Equip_WrongTypeWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Weapon testBow = new Weapon
            {
                ItemName = "testBow",
                ItemSlot = "Weapon",

                WeaponType = "Bow",
                Damage = 11,
                AttackSpeed = 0.8
            };

            //Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testBow.TryEquipWeapon(warrior1));
        }

        //Test case 4:
        [Fact]
        public void Equip_WrongTypeArmour_ShouldThrowInvalidArmourException()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Armour testCloth = new Armour
            {
                ItemName = "testCloth",
                ItemSlot = "Body",

                ArmourType = "Cloth",
                VitalityBonus = 5,
                StrengthBonus = 5,
                DexterityBonus = 0,
                IntelligenceBonus = 0

            };

            //Act + Assert
            Assert.Throws<InvalidArmourException>(() => testCloth.TryEquipArmour(warrior1));
        }

        //Test case 5:
        [Fact]
        public void Equip_CompatibleWeapon_ShouldReturnWeaponEquippedIsTrue()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Weapon testAxe = new Weapon
            {
                ItemName = "testAxe",
                ItemSlot = "Weapon",

                WeaponType = "Axe",
                Damage = 11,
                AttackSpeed = 0.8
            };

            string equipSucces = "New weapon equipped!";
            string expected = equipSucces;

            //Act
            testAxe.TryEquipWeapon(warrior1);
            string actual = warrior1.ReturnWeaponEquipped();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 6:
        [Fact]
        public void Equip_CompatibleArmour_ShouldReturnArmourEquippedIsTrue()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Armour testPlate = new Armour
            {
                ItemName = "testPlate",
                ItemSlot = "Body",

                ArmourType = "Plate",
                VitalityBonus = 5,
                StrengthBonus = 5,
                DexterityBonus = 0,
                IntelligenceBonus = 0

            };

            string equipSucces = "New armour equipped!";
            string expected = equipSucces;

            //Act
            testPlate.TryEquipArmour(warrior1);
            string actual = warrior1.ReturnBodyEquipped();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 7:
        [Fact]
        public void Get_WarriorBaseDPS_ShouldReturn1Dot05()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            double baseDPS = 1.05;
            double expected = baseDPS;
            //Act
            warrior1.CalculateDPS();
            double actual = warrior1.ReturnBaseDPS();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 8:
        [Fact]
        public void Get_WarriorWeaponDPS_ShouldReturn9Dot24()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Weapon testAxe = new Weapon
            {
                ItemName = "testAxe",
                ItemSlot = "Weapon",

                WeaponType = "Axe",
                Damage = 11,
                AttackSpeed = 0.8
            };

            double weaponDPS = 9.24;
            double expected = weaponDPS;
            //Act
            testAxe.TryEquipWeapon(warrior1);
            warrior1.CalculateDPS();
            double actual = warrior1.ReturnWeaponDPS();
            //Assert
            Assert.Equal(expected, actual);
        }

        //Test case 8:
        [Fact]
        public void Get_WarriortotalDPS_ShouldReturn9Dot68()
        {
            //Arrange
            Warrior warrior1 = new Warrior
            {
                Name = "Marcus",
                CharacterClass = "Warrior",

                VitalityBase = 10,
                StrengthBase = 5,
                DexterityBase = 2,
                IntelligenceBase = 1,

                VitalityPerLevel = 5,
                StrengthPerLevel = 3,
                DexterityPerLevel = 2,
                IntelligencePerLevel = 1
            };

            Weapon testAxe = new Weapon
            {
                ItemName = "testAxe",
                ItemSlot = "Weapon",

                WeaponType = "Axe",
                Damage = 11,
                AttackSpeed = 0.8
            };

            Armour testPlate = new Armour
            {
                ItemName = "testPlate",
                ItemSlot = "Body",

                ArmourType = "Plate",
                VitalityBonus = 5,
                StrengthBonus = 5,
                DexterityBonus = 0,
                IntelligenceBonus = 0

            };

            double totalDPS = 9.68;
            double expected = totalDPS;
            //Act
            testAxe.TryEquipWeapon(warrior1);
            testPlate.TryEquipArmour(warrior1);
            warrior1.CalculateDPS();
            double actual = warrior1.ReturnTotalDPS();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}